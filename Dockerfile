FROM kalilinux/kali-rolling

ARG PACKAGE

RUN apt-get update && \
    apt-get full-upgrade -y && \
    apt-get install -y ${PACKAGE} && \
    rm -rf /var/lib/apt/lists/*

ARG ENTRYPOINT

ENTRYPOINT $ENTRYPOINT
