# Kali from Docker

Add the following function to your `.bashrc`, and then run each tool as `kali <tool>`, i.e. `kali nmap 192.168.1.1`

```shell
kali () { docker run --rm -ti -v "$PWD":/pwd --workdir /pwd registry.gitlab.com/gereedschap/kali:"$1" "$@"; }
```

Your current working directory will be mapped in the container as well, so you can also access files directly. For example `kali john ./passwords.txt`.


If you'd like to keep the image up to date, add for example the following extra function:

```shell
kalii () { docker pull registry.gitlab.com/gereedschap/kali:"$1" && docker run --rm -ti -v "$PWD":/pwd --workdir /pwd registry.gitlab.com/gereedschap/kali:"$1" "$@"; }
```

Then each run will pull the latest version before running.

## Why though?

This allows you to
- easily stay up to date on each tool
- use them on any system which runs Docker, even Kali of course!
- have small images without having to download all the tools. The first layer is shared between images, so you'd only retrieve the bytes of the tool you need.
