#!/usr/bin/env python
import yaml
from jinja2 import Template

with open("packages.yml", "r") as stream:
    packages = yaml.safe_load(stream)

package_names = {k for d in packages["top10"] for k in d.keys()}

if __name__ == "__main__":
    with open("template.yml", "r") as f:
        print(
            Template(f.read()).render(
                {"packages": packages["top10"], "package_names": package_names}
            )
        )
